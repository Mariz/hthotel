<!DOCTYPE html>
<html>
<head>
  <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Hometown Hotel Makati</title>
  <!-- google fonts roboto regular -->
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' type='text/css'>
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lora:400,700' type='text/css'>
  <link rel="stylesheet" href="css/owl.carousel.css">
  <!-- <link rel="stylesheet" href="css/owl.theme.default.min.css"> -->
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/jquery-ui.structure.css">
  <link rel="stylesheet" href="css/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="css/jquery.timepicker.css">
  <link rel="stylesheet" href="css/jquery.countdown.css">
  <link rel="stylesheet" href="css/flexslider.css">
  <link rel="stylesheet" href="css/red.css">
  <link rel="stylesheet" href="css/select2.css">
  <link rel="stylesheet" href="css/jquery.raty.css" />
  <link rel="stylesheet" href="css/hotel.style1.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
</head>
<style>
 .navbar-brand{
  margin-top: -22px;
  margin-right: 5px;
}
.btn-primary {
    color: #fff;
    background-color: #96281B;
    border-color: #96281B;
}
.container{
  margin-top: 40px;
}
.row{
  padding-right: 15px;
  padding-left: 15px;
}
h1{
  font-size: 30;
}
.container, h1{
  text-transform: uppercase;
}
</style>
<body>
  <header>
    <nav class="navbar rq-header-main-menu navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <!-- Navbar Toggle -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" style = "margin-right: 20px;">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <!-- Logo -->
          <a class="navbar-brand" href="index.php"><img class="logo" src="HometownLogo.png" alt="Hometown"></a>
        </div>
        <!-- Navbar Toggle End -->

        <!-- navbar-collapse start-->
        <div id="nav-menu" class="navbar-collapse rq-menu-wrapper collapse navbar-right" role="navigation">
          <ul class="nav navbar-nav rq-menus">
            <li class="active">
              <a href="index.php">Home</a>
            </li>

              <a href="Rooms.php">Room</a>
              </li>
              <li>
                <a href="aboutUs.php">About</a>
              </li>
              <li>
                <a href="ContactUs.php">Contact</a>
              </li>
              <li>
                <a href="Step1.php">Reservations</a>
              </li>
              <?php if(isset($_SESSION['login'])){  ?>
              <li class="active dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded = "false">
                  MY ACCOUNT<span class="caret"></span>
                </a>
                <ul class="rq-sub-menu">
                  <li><a href="GuestDashboard.php">MY RESERVATIONS</a></li>
                  <li><a href="AccountSettings.php">ACCOUNT SETTINGS</a></li>  
                  <li><a href="Logout.php">LOGOUT</a></li>
                </ul>
              </li>
              <?php } else { ?> 
              <li><a href="login.php">LOGIN</a></li>
              <?php } ?>   

            </ul>
          </div>
        </div>
      </nav>
    </header>
    <br>
    <br>
    <br>
    <br>
    <br>
  <div class = "container-fluid">
    <div class = "row">
      <div class = "col-lg-6" style=" height:100%; width:100%; padding-top:50px; padding-bottom:50px;"> 
        <CENTER><p style="color:black; font-size:20px;">Please wait your Reservation to verified<i>.</i></p></CENTER>
        <a href = "DepositSlip.<?php  ?>"><center><p style="font-size:15px; color:black; text-decoration: underline;">Go back to home<i class="fa fa-home"></i></p></center></a>
      </div>    
      </div>
      </div>
    </div>

</body>
</html>