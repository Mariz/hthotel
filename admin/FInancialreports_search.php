<?php
include "../db.php";

$start = $_GET['start'];
$end = $_GET['end'];
$month = $_GET['month'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Financial Report Print</title>

    <style>
        img {
            /* float: left; */
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 7%; 
        }
        table.center {
            margin-left:auto; 
            margin-right:auto;
        }
        .center {
            text-align: center;
        }
        /* h3, span {
            float:right;
        } */
    </style>
</head>
<body>
    <div class = "center">
        <p>
            <img src="../logo.jpeg" alt="Hometown Hotel">
            <b>Hometown Hotel - Makati Report</b>
            <br>
            <span>57 Epifanio de los Santos Ave, Makati City</span>
            <br>
            <span>Hotel Phone Office: (02) 805 3386</span>
            <br>
            <span>hometownhotelmakati@gmail.com</span>  <br><br> 
        </p>   
    </div> 
    <hr/>
    <h2><b>Reports on Financial from: <?php echo "$start" . " to " ." $end"; ?></b></h2>
    <hr/>
</body>

<?php
$fetch_all_reservation = $conn->query("SELECT * FROM financialreports_masterfile 
JOIN billing_masterfile ON billing_masterfile.billing_id = billing_masterfile.billing_id
JOIN guest_masterfile ON guest_masterfile.guest_id = billing_masterfile.guest_id
JOIN reservation_masterfile ON reservation_masterfile.reservation_id = billing_masterfile.reservation_id
WHERE checkindate BETWEEN '$start' AND '$end' ");
   
?>
<table class ='ui striped table'>
<thead>
    <tr>
    <th>Financial Report ID</th>
    <th>Payment</th>
    <th>Payment Type</th>
    <th>Created at</th>
    </tr>
</thead>
<tbody>
<?php
$total = 0;
while ($rows = mysqli_fetch_assoc($fetch_all_reservation)) {
    $financialreport_id = $rows['financialreport_id'];
    $payment= number_format($rows['payment'],2);
    $type = $rows['payment_type'];
    $created_At = $rows['created_at'];
    $total += $rows['payment'];

?>
<tr>
<td><?php echo $financialreport_id; ?></td>
<td><?php echo $payment; ?></td>
<td><?php echo $type; ?></td>
<td><?php echo $created_At; ?></td>

</tr>

<?php
}

?>

</tbody>
<tfoot></tfoot>
</table>

<h3>
    <?php echo "Total Earnings: ". number_format($total,2);?>
</h3>
<p style="text-align:right; font-size:20px; margin-top:50px;"id="report-footer">Printed by: _________________</p>
    <!-- <script>
        window.print();
    </script> -->
  <!-- Bootstrap Core CSS -->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Custom CSS
<link href="../dist/css/sb-admin-2.css" rel="stylesheet"> -->
</body>
</html>

