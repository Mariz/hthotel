<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HomeTown Hotel Makati - Admin</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">
    <?php include 'layout/navbar.php';?>
     

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Financial Reports</h1>
                    </div>

                    <!-- Start Here -->
                    <div class ='card' style ='margin-bottom:50px'>
                        <div class ='card-header'>
         
                            
                        </div>
                        <div class ='card-body'>
                            <div class ='row'>
                            <div class = 'form-group col-md-4'>
                                <label>Start Date </label>
                                <input type = "date" id = "start" class = "form-control">
                            </div>
                            <div class = 'form-group col-md-4'>
                                <label>End Date </label>
                                <input type = "date" id = "end" class = "form-control">
                            </div>

                            <!-- // <div class ='form-group col-md-4'>
                            //     <label>Month</label>
                            //     <select class ='form-control' name ='month'>
                            //     <?php 
                            //     echo "<option value ='None'>None</option>";
                            //     $months = array("January","February","March","April","May","June","July","August","September","October","November","December");
                            //     foreach($months as $i => $month){
                            //         echo "<option value ='".($i+1)."'>{$month}</option>";
                            //     }
                            //     ?>
                            //     </select>
                            // </div>
                            // <div class ='form-group col-md-4'>
                            //     <label>Year</label>
                            //     <select class ='form-control' name ='year'>
                            //     <?php 
                            //     $currentYear = intval(date("Y"));
                            //     echo "<option value ='None'>None</option>";
                            //     for($i = $currentYear; $i>= 1980; $i--){
                            //         echo "<option value ='{$i}'>{$i}</option>";
                            //     }
                            //     ?>
                            //     </select>
                            // </div>
                            // <div class ='form-group col-md-4'>
                            //     <label>Day</label>
                            //     <select class ='form-control' name ='day'>
                            //     <?php 
                            //     echo "<option value ='None'>None</option>";
                            //     $currentYear = intval(date("Y"));
                            //     for($i = 1; $i <= 31; $i++){
                            //         echo "<option value ='{$i}'>{$i}</option>";
                            //     }
                            //     ?>
                            //     </select>
                            // </div>
                            // </div> -->
                            <br>
                        <button class ='btn btn-success printbtn' style ='margin-top:24px' onclick="generateReport();">Search</button> 
                        <a onclick="printTable();" style ='margin-top:24px' target ='_blank' class ='btn btn-success printbtn'>Print</a>
                        <div class="report-search">

                       </div>
                        </div>
                        </div>
                        <!-- <h5 id ='totalBill'>Total:</h5> -->


                        <hr/>
                                                   

                                                   
                       <!-- <table id ='thisTable' class ='table table-striped display dataTable table-responsive'>
                            <thead>
                                <tr>
                                <th>Financial report ID</th>
                                <th>Payment</th>
                                <th>Payment Type</th>
                                <th>Created_at</th>
                                </tr>
                            </thead>
                        </table> -->

                        
                        
                        <footer class="sticky-footer">
                        <div class="container">
                            <div class="text-center">
                            <small>Copyright © HomeTown Hotel Makati
                            </small>
                            </div>
                        </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script>
      function printTable() {
                        var start = $("#start").val();
                        var end = $("#end").val();
                       var win =  window.open(`FInancialreports_search.php?start=${start}&end=${end}&print&month`,'_blank');
                        win.focus();
                    }

     function generateReport()
        {
            var start = $("#start").val();
            var end = $("#end").val();
            $(".report-search").load(`FInancialreports_search.php?&start=${start}&end=${end}&month=`);
        }
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

<!--datepicker-->
<script src = "../vendor/datepicker/jquery.datetimepicker.full.min.js"></script>
<script src = '../js/edit_reservation.js'>
</script>
<script>
  $(document).ready(function(){

    var total = 0
    $('td#guest-id').each(function(){
      total += parseFloat($(this).html().replace(/\,/, ''))
    }
    )
    $('#totalBill').html(`Total Earnings: ${total}
      .00 PHP`)
    var table = $('#thisTable').DataTable({
 "order": [[ 3, "desc" ]]
    })
    $('#thisTable_filter').hide()
    $('select[name=month], select[name=year], select[name=day]').change(function(){
        var month = $('select[name=month]').val(), year = $('select[name=year]').val(), day = $('select[name=day]').val()
      $.ajax({
        type:'POST',
        url:'../ajax/filterfinancial.php',
        data:{
          month: month,
          year: year,
          day: day,
          dbtype:"financialreports_masterfile"
        },
        success:function(html){
          table.clear().draw()
          $('.printbtn').attr('href', `printreport.php?category=financial&year=${year}&month=${month}&day=${day}`)
          $('#financialreports').html(html)

          total = 0
          $('td#guest-id').each(function(){
            total += parseFloat($(this).html().replace(/\,/, ''))
          }
          )
          $('#totalBill').html(`Total Earnings: ${total}
            .00 PHP`)

        }
      })
    })
    $('form#deleteproof').on("submit",function(){
      var prompt = confirm("Are you sure?")
      if(prompt){
        $.ajax({
          url:'../ajax/deleteproof.php',
          type:'POST',
          data:$(this).serialize(),
          success:function(html){
            alert("Proof of payment has been deleted")
            location.reload()
          }
        }
        )
      }
    }
    )
    $('a.edit').on('click',function(){
      var imagepath = $(this).closest('tr').find('img').attr('src')
      var proof_id = $(this).closest('tr').find('#reservation-id').html()
      $('input[name=old_img').val(imagepath)
      $('input[name=img').val(imagepath)
      $('input[name=t_id]').val(proof_id)
    }
    )
    $('form#editproof').on("submit",function(){
      var form_data = new FormData()
      form_data.append('img', $('input[name=img]').prop('files')[0])
      form_data.append('t_id', $('input[name=t_id]').val())
      form_data.append('old_img', $('input[name=old_img]').val())
      $.ajax({
        url:'../ajax/editproof.php',
        type:'POST',
        data:form_data,
        contentType: false,
        processData: false,
        success: function(html){
          alert("Success")
          location.reload()
        }
      }
      )
    }
    )
  }
  )
</body>

</html>
