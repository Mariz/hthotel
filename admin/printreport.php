<?php 
include '../config/db.php';
require ('../fpdf.php');

$pdf = new FPDF();
$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$condstr ='';
if(isset($_GET['year']) || isset($_GET['month']) || isset($_GET['day'])){
if($_GET['year'] != 'None'){
    if($condstr != '')
        $condstr .= " AND YEAR(created_at) = {$_GET['year']}";
    else
        $condstr .= "YEAR(created_at) = {$_GET['year']}";
}
if($_GET['month'] != 'None'){
    if($condstr != '')
        $condstr .= " AND MONTH(created_at) = {$_GET['month']}";
    else
        $condstr .= "MONTH(created_at) = {$_GET['month']}";
}
if($_GET['day'] != 'None'){
    if($condstr !='')
        $condstr .= " AND DAY(created_at) = {$_GET['day']}";
    else
        $condstr.="DAY(created_at) = {$_GET['day']}";
}
if($condstr != ''){
    $condstr = "WHERE " . $condstr;
}
}
if(isset($_GET['category'])){
	if($_GET['category'] == 'financial'){
		$total = 0;
		$query = mysqli_query($conn, "SELECT * FROM financialreports_masterfile {$condstr}");
		// Column Dito mo lagay yung header tapos after nun lagyan mo ng $pdf->Ln() para line break
		
		
		$pdf->SetFont('Times','B',16);
		$pdf->Cell(0,10,"Hometown Hotel - Makati", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"57 Epifanio de los Santos Ave, Makati", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"(02) 805 3386", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"hometownhotelmakati@gmail.com", 0, 0, 'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Times','B',14);
		$pdf->Cell(0,5,"Income Report", 0, 0, 'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Times','',12);
		$pdf->Cell(45,7,"Financial report ID",1);
		$pdf->Cell(45,7,"Payment", 1);
		$pdf->Cell(45,7,"Payment Type", 1);
		$pdf->Cell(50, 7,"Created_At", 1);
		$pdf->Ln();
		while($row = mysqli_fetch_assoc($query)){
			$pdf->Cell(45,7,$row['financialreport_id'],1);
			$pdf->Cell(45,7,number_format($row['payment'],2) . " PHP",1);
			$pdf->Cell(45,7,$row['payment_type'],1);
			$pdf->Cell(50,7,$row['created_at'],1);
			$pdf->Ln();
			$total+= $row['payment'];
		}
		$pdf->Ln();
		$pdf->Cell(45,7,"Total: ");
		$pdf->Cell(45,7,number_format($total,2) . " PHP");
		$pdf->Output();
	}
	else if($_GET['category']=='reservation'){
		$query = mysqli_query($conn, "SELECT * FROM reservationreports_masterfile {$condstr}") or die(mysqli_error($conn));
		// same rin dito
		$pdf->SetFont('Times','B',16);
		$pdf->Cell(0,10,"Hometown Hotel - Makati", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"57 Epifanio de los Santos Ave, Makati", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"(02) 805 3386", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"hometownhotelmakati@gmail.com", 0, 0, 'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Times','B',14);
		$pdf->Cell(0,5,"Reservation Report", 0, 0, 'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(25,7,"Report ID",1);
		$pdf->Cell(55,7,"Guest Name", 1);
		$pdf->Cell(20,7,"Status", 1);
		$pdf->Cell(20,7,"Type",1);
		$pdf->Cell(35,7,"Created at",1);
		$pdf->Cell(35,7,"Updated at",1);
		$pdf->Ln();
		while($row = mysqli_fetch_assoc($query)){
		                $fetch = array();
        if($row['type'] =='reserve'){
            $getinfo = mysqli_query($conn, "SELECT * FROM reservation_masterfile INNER JOIN guest_masterfile ON guest_masterfile.guest_ID = reservation_masterfile.guest_id WHERE reservation_masterfile.reservation_id = {$row['reservation_id']}") or die(mysqli_error($conn));
            $fetch = mysqli_fetch_assoc($getinfo);
            $row['guest_firstname'] = $fetch['guest_firstname'];
            $row['guest_lastname'] = $fetch['guest_lastname'];
            $row['status'] = $fetch['status'];
        }
        else if($row['type'] =='walkin'){
            $getinfo = mysqli_query($conn, "SELECT * FROM walkinreservation_masterfile WHERE reservation_id = {$row['reservation_id']}");
            $fetch = mysqli_fetch_assoc($getinfo);
            $row['guest_firstname'] = $fetch['firstname'];
            $row['guest_lastname'] = $fetch['lastname'];
            $row['status'] = $fetch['status'];
        }
			$pdf->Cell(25,7,$row['reservereports_id'],1);
			$pdf->Cell(55,7,"{$row['guest_firstname']} {$row['guest_lastname']}",1);
			$pdf->Cell(20,7,$row['status'],1);
			$pdf->cell(20,7,$row['type'],1);
			$pdf->Cell(35,7,$row['created_at'],1);
			$pdf->Cell(35,7,$row['updated_at'],1);
			$pdf->Ln();
		}		$pdf->Output();
	}
	else if($_GET['category'] == 'discount'){
	    $query = mysqli_query($conn, "SELECT * FROM guest_masterfile WHERE count >= 5");
	    $pdf->SetFont('Times','B',16);
		$pdf->Cell(0,10,"Hometown Hotel - Makati", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"57 Epifanio de los Santos Ave, Makati", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"(02) 805 3386", 0, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(0,5,"hometownhotelmakati@gmail.com", 0, 0, 'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Times','B',14);
		$pdf->Cell(0,5,"Reservation Report", 0, 0, 'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(45,7,"Name", 1);
		$pdf->Cell(40,7,"Email",1);
		$pdf->Cell(30,7,"Contact Number",1);
		$pdf->Cell(40,7,"Country",1);
		$pdf->Cell(41,7,"Address",1);
		$pdf->Ln();
		while($row = mysqli_fetch_assoc($query)){
		    $getlength = ceil(strlen($row['guest_address'])/28) * 7;
		    $pdf->Cell(45, $getlength, $row['guest_firstname']. " " . $row['guest_lastname'], 1);
		    $pdf->Cell(40, $getlength, $row['guest_email'],1);
		    $pdf->Cell(30, $getlength, $row['guest_contactNumber'],1);
		    $pdf->Cell(40,$getlength, $row['guest_country'],1);
		    $pdf->MultiCell(41,7,$row['guest_address'],1);
		}		$pdf->Output();
	}
}

?>