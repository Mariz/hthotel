<?php
include '../config/db.php';

if(isset($_POST['submit'])) {
  $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
  $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
  $email = mysqli_real_escape_string($conn, $_POST['email']);
  $contactNumber = mysqli_real_escape_string($conn, $_POST['contactNumber']);
  $password = mysqli_real_escape_string($conn, $_POST['password']);
  $confirmPassword = mysqli_real_escape_string($conn, $_POST['confirmPassword']);
  $address = mysqli_real_escape_string($conn, $_POST['address']);
  $country = mysqli_real_escape_string($conn, $_POST['country']);

  if($password == $confirmPassword) {
    $hashedpassword = password_hash($password, PASSWORD_DEFAULT);
    $edit = $conn->query("UPDATE guest_masterfile SET guest_firstname = '$firstName', 
    guest_lastname = '$lastName', guest_email = '$email', guest_password = '$hashedpassword',
    guest_contactNumber = '$contactNumber', guest_address = '$address', guest_country = '$country'
    WHERE guest_ID = '{$_POST['guestID']}'");
    echo "<script>alert('Guest information has been updated')</script>";
  }
  else {
    echo "<script>alert('Password does not match')</script>";
  }

}

if (isset($_POST['delete'])) {
  $delete_guest_query =  "DELETE FROM guest_masterfile WHERE guest_ID = {$_POST['delete_id']}";
  try 
  {
    $delete_result = mysqli_query($conn, $delete_guest_query) or die (mysqli_error($conn) . "saan?");
    if ($delete_result) 
    {
      if (mysqli_affected_rows($conn) > 0) 
      {
        echo "<script>location.reload()</script>";
      }
    } 
  } 
  catch (Exception $ex) 
  {
    echo "Error Delete Data".$ex->getMessage();
  }
}
$sql = "SELECT * FROM guest_masterfile";
$result = $conn->query($sql);

	
?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">
    <?php include 'layout/navbar.php';?>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Guest Accounts</h1>
                    </div>
                    <!-- Start Here -->

                      <?php
                        if ($result->num_rows > 0) {
                          echo '
                              <div class="content-wrapper">
                                  <div class="container-fluid">
                                <h6>Guest Accounts</h6>
                                          <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="dataTable" align="center">
                                      <tr>
                                        <th>Guest ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email Address</th>
                                        <th>Contact Number</th>
                                        <th>Address</th>
                                        <th>Country</th>
                                        <th>Actions</th>
                                      </tr>';
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                          $guestID = $row['guest_ID'];
                                          $guest_firstname = $row["guest_firstname"];
                                          $guest_lastname = $row['guest_lastname'];
                                          $guest_email = $row['guest_email'];
                                          $guest_contactNumber =  $row['guest_contactNumber'];
                                          $guest_address = $row["guest_address"];
                                          $guest_country = $row["guest_country"];
                                            echo "<tr>
                                                  <td>" . $guestID . "</td>
                                                  <td>" . $guest_firstname . "</td>
                                                  <td>" . $guest_lastname . "</td>
                                                  <td>" . $guest_email . "</td>
                                                  <td>" . $guest_contactNumber . "</td>
                                                  <td>". $guest_address . "</td>
                                                  <td>" . $guest_country ."<td>
                                                      
                                                    <form method = 'POST' action = 'guestmodify.php'>
                                                      <input type ='hidden' value = '$guestID' name = 'delete_id'>
                                                      <button class = 'btn btn-info btn-xs' name = 'edit' type = 'button' data-toggle='modal' data-target= \"#editGuestAccount$guestID\">Edit</button>
                                                      <br><br>
                                                      <button name = 'delete' class = 'btn btn-info btn-xs delete_data' type = 'submit' onclick = \"confirm('Are you sure?')\")>Delete</button>
                                                    </form>
                                                  </td>";
                                                  ?>
                                                  <div class='modal fade' id='editGuestAccount<?php echo $guestID;?>' role='dialog'>
                                                    <div class='modal-dialog modal-lg'>
                                                      <div class='modal-content'>
                                                        <div class='modal-header'>
                                                          <h4 class='modal-title'>Update Infomation</h4>
                                                          <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                        </div>
                                                        <div class='modal-body'>
                                                          <form method = "POST" action = "">
                                                            
                                                            <input type="hidden" name="guestID"  value = "<?php echo $guestID; ?>" >
                                                            <div class="form-group">
                                                              <div class="form-row">
                                                                <div class="col-md-6">
                                                                  <label>First name</label>
                                                                  <input  required class="form-control" name = "firstName" type="text" aria-describedby="nameHelp" pattern = "[a-z A-Z]+" onkeypress="return isLetter(event)" value="<?php echo $guest_firstname;?>">
                                                                </div>
                                                                <div class="col-md-6">
                                                                  <label>Last name</label>
                                                                  <input required class="form-control" name = "lastName" type="text" aria-describedby="nameHelp" pattern = "[a-z A-Z ]+" onkeypress="return isLetter(event)" value="<?php echo $guest_lastname;?>">
                                                                </div>
                                                              </div>
                                                            </div>
                                                            <div class="form-group">
                                                              <div class="form-row">
                                                                <div class="col-md-6">
                                                                  <label>Email address</label>
                                                                  <input required class="form-control" name = "email" type="email" aria-describedby="emailHelp" value="<?php echo $guest_email;?>">
                                                                </div>
                                                                <div class="col-md-6">
                                                                  <label>Contact Number</label>
                                                                  <input required class="form-control" name = "contactNumber" type="number" aria-describedby="emailHelp" value="<?php echo $guest_contactNumber;?>">
                                                                </div>
                                                              </div>
                                                            </div>
                                                            <div class="form-group">
                                                              <div class="form-row">
                                                                <div class="col-md-6">
                                                                  <label>Password</label>
                                                                  <input required class="form-control" name = "password" type="password">
                                                                </div>
                                                                <div class="col-md-6">
                                                                  <label>Confirm Password</label>
                                                                  <input required class="form-control" name = "confirmPassword" type="password">
                                                                </div>
                                                              </div>
                                                            </div>
                                                            <div class="form-group">
                                                              <div class="form-row">
                                                                <div class="col-md-6">
                                                                  <label>Address</label>
                                                                  <input required class="form-control" name = "address" type="text" value="<?php echo $guest_address;?>">
                                                                </div>
                                                                <div class="col-md-6">
                                                                  <label for="country">Country</label><br>
                                                                  <select required name="country" class="form-control" value = "<?php echo $guest_country; ?>">
                                                                    <option value="Australia" <?php if ($country == 'Australia')  echo 'selected = "selected"'; ?>>Australia</option>
                                                                    <option value="China" <?php if ($country == 'China')  echo 'selected = "selected"'; ?>>China</option>
                                                                    <option value="HongKong" <?php if ($country == 'Hongkong')  echo 'selected = "selected"'; ?>>Hong Kong</option>
                                                                    <option value="India" <?php if ($country == 'India')  echo 'selected = "selected"'; ?>>India</option>
                                                                    <option value="Indonesia" <?php if ($country == 'Indonesia')  echo 'selected = "selected"'; ?>>Indonesia</option>
                                                                    <option value="Japan" <?php if ($country == 'Japan')  echo 'selected = "selected"'; ?>>Japan</option>
                                                                    <option value="Korea" <?php if ($country == 'Korea')  echo 'selected = "selected"'; ?>>Korea</option>
                                                                    <option value="Malaysia" <?php if ($country == 'Malaysia')  echo 'selected = "selected"'; ?>>Malaysia</option>
                                                                    <option value="Philippines" <?php if ($country == 'Philippines')  echo 'selected = "selected"'; ?>>Philippines</option>
                                                                    <option value="Saudi Arabia" <?php if ($country == 'Saudi Arabia')  echo 'selected = "selected"'; ?>>Saudi Arabia</option>
                                                                    <option value="Thailand" <?php if ($country == 'Thailand')  echo 'selected = "selected"'; ?>>Thailand</option>
                                                                    <option value="UAE" <?php if ($country == 'UAE')  echo 'selected = "selected"'; ?>>United Arab Emirates</option>
                                                                    <option value="UK" <?php if ($country == 'UK')  echo 'selected = "selected"'; ?>>United Kingdom</option>
                                                                    <option value="USA" <?php if ($country == 'USA')  echo 'selected = "selected"'; ?>>United States</option>
                                                                    <option value="Vietnam" <?php if ($country == 'Vietnam')  echo 'selected = "selected"'; ?>>Vietnam</option>
                                                                    <option value="Others" <?php if ($country == 'Others')  echo 'selected = "selected"'; ?>>Others</option>
                                                                  </select>
                                                                </div>
                                                              </div>
                                                            </div>
                                                            <input class="btn btn-primary btn-block" name = "submit" type = "submit" value = "Update Customer Account">                                                     
                                                        </form>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                <?php
                                        }
                                    echo "</table>
                                  </div>
                              </div>
                            </div>";
                      }
                      ?>                 
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script> -->
  <!-- <script type="text/javascript" language="javascript" >
    $(document).ready(function(){
      $(".edit_data").click(function(){
        var guest_id = $(this).closest("tr").find("#guest_id").html();
        var guest_firstname = $(this).closest("tr").find("#guest_firstname").html();
        var guest_lastname = $(this).closest("tr").find("#guest_lastname").html();
        var guest_email = $(this).closest("tr").find("#guest_email").html();
        var guest_contactNumber = $(this).closest("tr").find("#guest_contactNumber").html();
        // var guest_oldPassword = $(this).closest("tr").find("#guest_oldPassword").html();
        // var guest_newPassword = $(this).closest("tr").find("#guest_newPassword").html();
        var guest_country = $(this).closest("tr").find("#guest_country").html();
        console.log(room_description);
        $('#editGuestAccount').find('.modal-title').html(guest_id);
        $('#editGuestAccount').find('input[name=firstName]').val(guest_firstname);
        $('#editGuestAccount').find('input[name=lastName]').val(guest_lastname);
        $('#editGuestAccount').find('input[name=email]').val(guest_email);
        $('#editGuestAccount').find('input[name=contactNumber]').val(guest_contactNumber);
        $('#editGuestAccount').find('input[name=oldPassword]').val(guest_oldPassword);
        $('#editGuestAccount').find('input[name=newPassword]').val(guest_newPassword);
        $('#editGuestAccount').find('input[name=country]').val(guest_country);
        
      });
      $("#formEditGuestAccount").submit(function(e){
        e.preventDefault(); // remove default function of form submit
        $.ajax({
          type: 'POST', // type of submission
          url: 'updateguestaccount.php', // where the form send the data HAHAHAH
          data: $(this).serialize(), // roomType=blabla&roomDescription=blabla&....
          success: function(response){ // eto ung response ng php na nilagay mo sa url
            alert(response)
            location.href="updateguestaccount.php";
          }
        });
      });
    });
  </script> -->
</body>

</html>
