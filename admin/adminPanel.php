<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">
    <?php include 'layout/navbar.php';?>
     

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    
                   <?php 
       $currentDay = (isset($_POST['date']))?  $_POST['date'] : date("Y-m-d");?>

        <div class="col-lg-12">
        <center>
            <h1 class="page-header" id = 'availablerooms'>List of available rooms (<?= $currentDay ?>)</h1>
        </center>
        </div>

      
       <form method ='post' id ='filter'>
           <div class ='col-md-6'>
           <div class ='card'>
            <div class ='card-header'>  
           <h3>Date</h3>
           </div>
           <div class ='card-body'>
           <div class ='col-md-6'>
           <input type ='text' class ='form-control' id = 'date' name ='date' value = '<?= (isset($_POST['date']))? $_POST['date'] : '' ?>'/>
           </div>
           </div>
           </div>
           </div>
       </form>
       <hr/>
    <table class ='table table-striped' >
        <thead>
            <th>Room type</th>
            <th>Room name</th>
        </thead>
        <tbody>
    <?php 

    $fetchrooms = mysqli_query($conn, "SELECT * from walkinrooms_masterfile JOIN room_masterfile ON walkinrooms_masterfile.room_id = room_masterfile.room_id") or die(mysqli_error($conn));
    while($row = mysqli_fetch_assoc($fetchrooms)){
        $fetchavailable = mysqli_query($conn, "SELECT * FROM assignedroom_masterfile WHERE room_id = {$row['walkinrooms_id']} AND date = '{$currentDay}' AND status = 'Reserved'") or die(mysqli_error($conn));
        if(mysqli_num_rows($fetchavailable) == 0){
        ?>
        <tr>
            <td class ='roomcount'><?= $row['room_type'] ?></td>
            <td><?= $row['walkinrooms_name'] ?></td>
        </tr>
        <?php 
        }
        } ?>
    </tbody>
    </table>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <!--datepicker-->
    <script src = "../vendor/datepicker/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">
$('#availablerooms').html("List of available rooms " + $('.roomcount').length)
//$('#dataTable').DataTable()
$('#date').datetimepicker({
timepicker: false,
format: "Y-m-d"
})
$('#date').change(function(){
$('#filter').submit()
})

tday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
tmonth=new Array("January","February","March","April","May","June","July","August","September","October","November","December");

function GetClock(){
var d=new Date();
var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getFullYear();
var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;

if(nhour==0){ap=" AM";nhour=12;}
else if(nhour<12){ap=" AM";}
else if(nhour==12){ap=" PM";}
else if(nhour>12){ap=" PM";nhour-=12;}

if(nmin<=9) nmin="0"+nmin;
if(nsec<=9) nsec="0"+nsec;

document.getElementById('clockbox').innerHTML=""+tday[nday]+", "+tmonth[nmonth]+" "+ndate+", "+nyear+" "+nhour+":"+nmin+":"+nsec+ap+"";
}

window.onload=function(){
GetClock();
setInterval(GetClock,1000);
}
</script>

</body>

</html>
