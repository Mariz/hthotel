<?php   
    // include "layout/navbar.php";
    include '../config/db.php';

    $start = $_GET['start'];
    $end = $_GET['end'];
    $month = $_GET['month'];

    $fetchguestdiscount = $conn->query(
        "SELECT * FROM guest_masterfile WHERE COUNT >= 5");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Discount Report Print</title>

    <style>
        img {
            /* float: left; */
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 7%;
        }
        table.center {
            margin-left:auto; 
            margin-right:auto;
        }
        .center {
            text-align: center;
        }
        /* h3, span {
            float:right;
        } */
    </style>
    
</head>
<body>
    <div class = "center">
        <p>
            <img src="../logo.jpeg" alt="Hometown Hotel">
            <b>Hometown Hotel - Makati Report</b>
            <br>
            <span>57 Epifanio de los Santos Ave, Makati City</span>
            <br>
            <span>Hotel Phone Office: (02) 805 3386</span>
            <br>
            <span>hometownhotelmakati@gmail.com</span>  <br><br> 
        </p>   
    </div>

    <hr/>
    <h2><b>Reports on Discounts from: <?php echo "$start" . " to " ." $end"; ?></b></h2>
    <hr/>
    <table class="ui striped table">
        <thead>
            <tr>
                <th>Guest Name</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th>Country</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
        <?php
        while ($rows = mysqli_fetch_assoc($fetchguestdiscount)) {
            $guest_name = $rows['guest_firstname'] . " " . $rows['guest_lastname'];
            $guest_email = $rows['guest_email'];
            $guest_contactNumber = $rows['guest_contactNumber'];
            $guest_country = $rows['guest_country'];
            $guest_address = $rows['guest_address'];
            ?>
        <tr>
            <td><?php echo $guest_name; ?></td>
            <td><?php echo $guest_email; ?></td>
            <td><?php echo $guest_contactNumber; ?></td>
            <td><?php echo $guest_country; ?></td>
            <td><?php echo $guest_address; ?></td>
        </tr>

        <?php
        }

        ?>

        </tbody>
        <tfoot></tfoot>
    </table>
    <p style="text-align:right; font-size:20px; margin-top:50px;"id="report-footer">Printed by: _________________</p>
    <!-- <script>
        window.print();
    </script> -->

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet"> -->
</body>
</html>
