<?php
include "../db.php";

$start = $_GET['start'];
$end = $_GET['end'];
$month = $_GET['month'];

// echo $start;
// echo $end;

// $fetchallreservation = mysqli_query($conn, "SELECT * FROM reservationreports_masterfile");

$fetch_all_reservation = $conn->query(
    "SELECT * FROM reservationreports_masterfile
    JOIN reservation_masterfile ON reservationreports_masterfile.reservation_id = reservation_masterfile.reservation_id
    JOIN guest_masterfile ON guest_masterfile.guest_ID = reservation_masterfile.guest_id
    -- JOIN walkinreservation_masterfile ON walkinreservation_masterfile.reservation_id = guest_masterfile.reservation_id
    WHERE checkindate BETWEEN '$start' AND '$end'");    

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reservation Report Print</title>

    <style>
        img {
            /* float: left; */
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 7%; 
        }
        table.center {
            margin-left:auto; 
            margin-right:auto;
        }
        .center {
            text-align: center;
        }
        /* h3, span {
            float:right;
        } */
    </style>
</head>
<body>
    <div class = "center">
        <p>
            <img src="../logo.jpeg" alt="Hometown Hotel">
            <b>Hometown Hotel - Makati Report</b>
            <br>
            <span>57 Epifanio de los Santos Ave, Makati City</span>
            <br>
            <span>Hotel Phone Office: (02) 805 3386</span>
            <br>
            <span>hometownhotelmakati@gmail.com</span>  <br><br> 
        </p>   
    </div> 
    <hr/>
    <h2><b>Reports on Reservation from: <?php echo "$start" . " to " ." $end"; ?></b></h2>
    <hr/>
</body>

<table class ='ui striped table'>
<thead>
    <tr>
    <th>Guest Name</th>
    <th>Status</th>
    <th>Type</th>
    <th>Check-in</th>
    <th>Check-out</th>
    <!-- <th>Created at</th> -->
    <th>Updated at</th>
    </tr>
</thead>
<tbody>
<?php
while ($rows = mysqli_fetch_assoc($fetch_all_reservation)) {
    $guest_name = $rows['guest_firstname'] . " " . $rows['guest_lastname'];
    $reservationStatus = $rows['status'];
    $type = $rows['type'];
    $checkin = $rows['checkindate'];
    $checkout = $rows['checkoutdate'];
    // $createdAt = $rows['created_at'];
    $updatedAt = $rows['updated_at'];
    ?>
<tr>
<td><?php echo $guest_name; ?></td>
<td><?php echo $reservationStatus; ?></td>
<td><?php echo $type; ?></td>
<td><?php echo $checkin; ?></td>
<td><?php echo $checkout; ?></td>
<!-- <td><?php //echo $createdAt; ?></td> -->
<td><?php echo $updatedAt; ?></td>

</tr>

<?php
}

?>

</tbody>
<tfoot></tfoot>
</table>
<p style="text-align:right; font-size:20px; margin-top:50px;"id="report-footer">Printed by: _________________</p>
    <!-- <script>
        window.print();
    </script> -->

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet"> -->
</body>
</html>
