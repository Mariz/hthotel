<?php
   include 'layout/navbar.php';
    
if (isset($_POST['submit'])){

    $selectedValue = $_POST['country'];

    $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);

    $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);

    $email = mysqli_real_escape_string($conn, $_POST['email']);

    $password = mysqli_real_escape_string($conn, $_POST['password']);

    $confirmPassword = mysqli_real_escape_string($conn, $_POST['confirmPassword']);

    $contactNumber = mysqli_real_escape_string($conn, $_POST['contactNumber']);

    $country = mysqli_real_escape_string($conn, $selectedValue);

    $address = mysqli_real_escape_string($conn, $_POST['address']);        

    $sql = "SELECT * FROM guest_masterfile WHERE guest_email='$email'";

    $result = mysqli_query($conn, $sql) or die (mysqli_error($conn));

    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck > 0) {

        echo "<script>alert('Email already taken.');location.href='registerCustomer.php';</script>";

    }

    else {//hashing the password

      if ($password != $confirmPassword) {

      echo "<script>alert('Password mismatch.');location.href='register.php';</script>";

  } 

  else 

  {

      $hashedPwd = password_hash($password, PASSWORD_DEFAULT);

      $sql = "INSERT INTO guest_masterfile (guest_firstname, guest_lastname, guest_email, guest_password, guest_ContactNumber, guest_country, guest_address) VALUES ('$firstName', '$lastName', '$email', '$hashedPwd', '$contactNumber', '$country', '$address')";

      mysqli_query($conn, $sql) or die(mysqli_error($conn));

      echo "<script>alert('Successful!');location.href='adminPanel.php';</script>";  

      }         

    }         

  }

?>






<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">    

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add Customer Account</h1>
                    </div>
                    <!-- Start Here -->
                    <!-- <div class="container"> -->

                      <div class="card card-register mx-auto mt-5">

                        <!-- <div class="card-header">Guest Registration</div> -->

                        <div class="card-body">

                          <form method = "POST" action = "registerCustomer.php">

                            <div class="form-group">

                              <div class="row">

                                <div class="col-md-5">

                                  <label for="exampleInputName">First name</label>

                                  <input  required class="form-control" id="exampleInputName" name = "firstName" type="text" aria-describedby="nameHelp" placeholder="Enter first name" pattern = "[a-z A-Z]+" onkeypress="return isLetter(event)">

                                </div>

                                <div class="col-md-5">

                                  <label for="exampleInputLastName">Last name</label>

                                  <input required class="form-control" id="exampleInputLastName" name = "lastName" type="text" aria-describedby="nameHelp" placeholder="Enter last name" pattern = "[a-z A-Z ]+" onkeypress="return isLetter(event)">

                                </div>

                              </div>
                              <div class="row">

                                <div class="col-md-5">

                                  <label for="exampleInputEmail1">Email address</label>

                                  <input required class="form-control" id="exampleInputEmail11" name = "email" type="email" aria-describedby="emailHelp" placeholder="Enter email">

                                </div>

                                <div class="col-md-5">

                                  <label for="exampleInputContactNum1">Contact Number</label>

                                  <input required class="form-control" id="exampleInputEmail1" name = "contactNumber" type="text" aria-describedby="emailHelp" placeholder="Enter Contact Number">

                                </div>

                              </div>
                              <div class="row">

                                <div class="col-md-5">

                                  <label for="exampleInputPassword1">Password</label>

                                  <input required class="form-control" id="exampleInputPassword1" name = "password" type="password" placeholder="Password">

                                </div>

                                <div class="col-md-5">

                                  <label for="exampleConfirmPassword">Confirm password</label>

                                  <input required class="form-control" id="exampleConfirmPassword" name = "confirmPassword" type="password" placeholder="Confirm password">

                                </div>

                              </div>
                              <div class="row">

                                <div class="col-md-5">

                                  <label for="Address">Address</label>

                                  <input required class="form-control" id="exampleAddress" name = "address" type="text" placeholder="Address">

                                </div>

                                <div class="col-md-5">

                                  <label for="country">Country</label><br>

                                  <select required name="country">

                                    <option value="Australia">Australia</option>

                                    <option value="China">China</option>

                                    <option value="HongKong">Hong Kong</option>

                                    <option value="India">India</option>

                                    <option value="Indonesia">Indonesia</option>

                                    <option value="Japan">Japan</option>

                                    <option value="Korea">Korea</option>

                                    <option value="Malaysia">Malaysia</option>

                                    <option value="Philippines" selected="selected">Philippines</option>

                                    <option value="Saudi Arabia">Saudi Arabia</option>

                                    <option value="Thailand">Thailand</option>

                                    <option value="UAE">United Arab Emirates</option>

                                    <option value="UK">United Kingdom</option>

                                    <option value="USA">United States</option>

                                    <option value="Vietnam">Vietnam</option>

                                    <option value="Others">Others</option>

                                  </select>

                                </div>

                              </div>
                              <br>
                              <button class="btn btn-primary col-md-10" name = "submit" type = "submit">Register Customer</button>

                            </div>

                          </form> 

                          <div class="text-center">

                          <a class="d-block small mt-3" href="adminPanel.php">Go Back</a>

                          <!-- <a class="d-block small" href="forgot-password.php">Forgot Password?</a> -->

                        </div>

                        </div>

                      </div>

                    <!-- </div> -->


                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
