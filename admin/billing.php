<?=
include 'layout/navbar.php';
//  include '../config/db.php';

if (isset($_POST['update'])) {
    echo "<script>alert('Success')</script>";
    $currentTime = date("Y-m-d H:i:s");
    $cBalance = mysqli_real_escape_string($conn, $_POST['currentBalance']);
    $payment = mysqli_real_escape_string($conn, $_POST['payment']);
    $fetchreservation = mysqli_query($conn, "SELECT * FROM billing_masterfile INNER JOIN reservation_masterfile ON reservation_masterfile.reservation_id = billing_masterfile.reservation_id WHERE reservation_masterfile.reservation_id = {$_POST['r_id']}") or die(mysqli_error($conn));
    $total = 0;
    while ($walkin = mysqli_fetch_assoc($fetchreservation)) {
        $total += $walkin['total'];
        $balance += $walkin['balance'];
    }
    $walkins = mysqli_fetch_assoc($fetchreservation);
    $success = true;
    if ($balance / $payment < 0.50) {
        $_POST = array();
        echo "<script>alert('Please pay up to 50%');</script>
	    ";
        $success = false;
    }
    if ($payment < $cBalance) {
        mysqli_query($conn, "UPDATE walkinreservation_masterfile SET balance = balance - {$payment} WHERE reservation_id = {$_POST['r_id']}") or die(mysqli_error($conn));
        mysqli_query($conn, "INSERT INTO financialreports_masterfile (payment, payment_type, created_at) VALUES({$payment},'Partial', '{$currentTime}')") or die(mysqli_error($conn));
        // mysqli_query($conn, "INSERT INTO financialreports_masterfile values()");
    } else {
        mysqli_query($conn, "UPDATE walkinreservation_masterfile SET balance = 0 WHERE reservation_id = {$_POST['r_id']}");
        mysqli_query($conn, "INSERT INTO financialreports_masterfile(payment, payment_type,created_at) VALUES({$cBalance}, 'Fully Paid', '{$currentTime}')") or die(mysqli_error($conn));
    }
    $_POST = array();
}
if (isset($_POST['updatewalkin'])) {

    $currentTime = date("Y-m-d H:i:s");
    $cBalance = mysqli_real_escape_string($conn, $_POST['currentBalance']);
    $payment = mysqli_real_escape_string($conn, $_POST['payment']);

    $fetchreservation = mysqli_query($conn, "SELECT * FROM walkinreservation_masterfile WHERE code = '{$_POST['r_id']}'") or die(mysqli_error($conn));
    $total = 0;
    while ($walkin = mysqli_fetch_assoc($fetchreservation)) {
        $total += $walkin['total'];
        $balance += $walkin['balance'];
    }
    $walkins = mysqli_fetch_assoc($fetchreservation);
    $success = true;
    if ($balance / $payment < 0.50 || $walkins['status'] == 'Pending') {
        $_POST = array();
        echo "<script>alert('Please pay up to 50%');</script>
	    ";
        $success = false;
    }
    $paid = $total - $balance;
    if (($total / $paid) >= 0.50) {
        mysqli_query($conn, "UPDATE walkinreservation_masterfile SET status = 'Approved' WHERE code = '{$_POST['r_id']}'");
    }
    if ($payment < $cBalance) {
        mysqli_query($conn, "UPDATE walkinbilling_masterfile SET balance = balance - {$payment} WHERE code= '{$_POST['r_id']}'") or die(mysqli_error($conn));
        mysqli_query($conn, "INSERT INTO financialreports_masterfile (payment, payment_type, created_at) VALUES({$payment},'Partial', '{$currentTime}')") or die(mysqli_error($conn));
        // mysqli_query($conn, "INSERT INTO financialreports_masterfile values()");
    } else {
        mysqli_query($conn, "UPDATE walkinbilling_masterfile SET balance = 0 WHERE code = '{$_POST['r_id']}'");
        mysqli_query($conn, "INSERT INTO financialreports_masterfile(payment, payment_type,created_at) VALUES({$cBalance}, 'Fully Paid', '{$currentTime}')") or die(mysqli_error($conn));
    }
    $_POST = array();
    if ($success) {
        echo "<script>alert('Success')</script>";
    }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Billing</h1>
                    </div>
                    <!-- Start Here -->
                    <div class ='card'>
                      <div class ='card-body' style ='padding:2px'>
                          <div class = 'row'>
                              <div class ='col-md-6'>
                                  <div class ='card card-body' id ='onlinecard' align ='center' style ='background-color:	#DC143C; padding: 14px;'>
                                <a href ='#' id = 'online' style ='color:white'>Online reservation</a>
                                </div>
                              </div>
                              <div class ='col-md-6'>
                                  <div class ='card card-body' id ='walkincard' align ='center' style = 'background-color: #FA8072; padding: 14px;'>
                                  <a href ='#' id ='walkin' style ='color:black'>Walkin</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div id = 'billingwalkin' style ='display:none;max-width:1064px'>
                    <table class ='table table-striped display dataTable thisTable' id ='tablewalkin' style ='width:100%;overflow-x:auto'>
                      <thead>
                      <tr>
                        <th>Reservation ID</th>
                        <th>Check in date</th>
                        <th>Check out date</th>
                        <th>Name</th>
                        <th>Balance</th>
                        <th>Total</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php $query = mysqli_query($conn, "SELECT *, walkinbilling_masterfile.balance as wbalance, walkinbilling_masterfile.total as wtotal FROM walkinbilling_masterfile JOIN walkinreservation_masterfile ON walkinbilling_masterfile.code = walkinreservation_masterfile.code") or die(mysqli_error($conn));
while ($row = mysqli_fetch_assoc($query)) {
    ?>
                          <tr>
                            <td id = 'reservation-id'><?=$row['code']?></td>
                            <td><?=$row['checkindate']?></td>
                            <td><?=$row['checkoutdate']?></td>
                            <td><?="{$row['firstname']} {$row['lastname']}"?></td>
                            <td id ='balance'><?=number_format($row['wbalance'], 2)?></td>
                            <td><?=number_format($row['wtotal'], 2)?></td>
                            <td><?=$row['email']?></td>
                            <?php $disabled = '';
    $btnname = 'Pay';
    if ($row['wbalance'] == 0) {
        $disabled = 'disabled';
        $btnname = 'Fully Paid';
    }
    ?>
                            <td><a href ='#'class ='btn btn-info btn-block paymentmodalwalkin <?=$disabled?>' data-toggle= 'modal' data-target='#paywalkin'><?=$btnname?></a>
                            <!--<a href = '#' class ='btn btn-info btn-block addservicewalkin' data-toggle = 'modal' data-target='#addservicewalkin'>Add service</a>-->
                            </td>
                          </tr>
                          <?php
}?>
                      </tbody>
                      <tfoot></tfoot>
                    </table>
                  </div>
                  <div id = 'billingreservation' style ='max-width:620px'>
                    <table class ='table table-striped ' id = 'tablebilling' style ='width:100%;overflow-x:auto'>
                        <thead>
                          <th style ='display:none'></th>
                          <th style ='display:none'></th>
                          <th>Billing ID</th>
                          <th style ='display:none'>Guest ID</th>
                          <th>Guest name</th>
                          <th>Reservation ID</th>
                          <th>Balance</th>
                          <th>Total</th>
                          <th>Downpayment</th>
                          <th>Status</th>
                          <th>Created_at</th>
                          <th>Updated_at</th>
                          <th>Actions</th>
                        </thead>
                        <tbody>
                            <?php
$fetchallreservation = mysqli_query($conn, "select * from billing_masterfile join guest_masterfile on billing_masterfile.guest_id = guest_masterfile.guest_ID join reservation_masterfile on reservation_masterfile.reservation_id = billing_masterfile.reservation_id JOIN room_masterfile on room_masterfile.room_id = reservation_masterfile.room_id WHERE reservation_masterfile.status != 'Checkout' AND reservation_masterfile.status != 'Void'") or die(mysqli_error($conn));
$currentTime = date("Y-m-d");
while ($row = mysqli_fetch_assoc($fetchallreservation)) {
    ?>
                              <tr>
                                <td id = 'checkin' style ='display:none'><?=$row['checkindate']?></td>
                                <td id = 'checkout' style = 'display:none'><?=$row['checkoutdate']?></td>
                                <td id ='billing-id' ><?=$row['billing_id']?></td>
                                <td id = 'guest-id' style ='display:none'><?=$row['guest_id']?></td>
                                <td><?="{$row['guest_firstname']} {$row['guest_lastname']}"?></td>
                                <td id = 'reservation-id' ><?=$row['reservation_id']?></td>
                                <td id = 'balance' ><?=number_format($row['balance'], 2)?></td>
                                <td id = 'total' ><?=number_format($row['total'], 2)?></td>
                                <td id = 'number-guest'><?=number_format($row['downpayment'], 2)?></td>
                                <td ><?=$row['status']?></td>
                                <td ><?=$row['created_at']?></td>
                                <td ><?=$row['updated_at']?></td>
                                <td><form class ='deletebilling' action = '../ajax/deletebilling.php'>
                                    <?php $disabled = '';
    if ($row['total'] != $row['balance']) {
        $disabled = 'disabled';

    }

    $btnname = 'Pay';
    $paydisabled = '';
    if ($row['balance'] == 0) {
        $btnname = 'Fully Paid';
        $paydisabled = 'disabled';
    }
    ?>
                                <a data-toggle ='modal' data-target = '#editreservation1' class='btn btn-primary btn-block editreservation <?=$disabled?>' style ='color:white;margin-bottom:10px'>Edit</a>
                                <a data-toggle ='modal' data-target = '#addservice' class ='btn btn-primary addservice btn-block' style ='color:white;margin-bottom:10px'>Add service</a>
                                <a data-toggle ='modal' data-target = '#pay' class='btn btn-primary btn-block paymentmodal <?=$paydisabled?>' style ='color:white;margin-bottom:10px'><?=$btnname?></a>
                                <input type="hidden" name="t_id" value="<?=$row['billing_id']?>">
                                <input type ='hidden' name ='decrease' value ='<?=floor(($row['number_guest'] - 1) / $row['room_capacity'])?>'/>
                              </form></td>
                            </tr>
                            <?php
}
?>
                        </tbody>
                    </table>
                  </div>
                  <footer class="sticky-footer">
                    <div class="container">
                      <div class="text-center">
                        <small>Copyright © HomeTown Hotel Makati</small>
                      </div>
                    </div>
                  </footer>
                  <!-- Scroll to Top Button-->
                  <a class="scroll-to-top rounded" href="#page-top">
                    <i class="fa fa-angle-up"></i>
                  </a>
                  <!-- Logout Modal-->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>
                          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                          <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" href="logout.php">Logout</a>
                          </div>
                        </div>
                      </div>
                    </div>

                  <!-- Pay WALKIN -->
                    <div class="modal fade" id ='paywalkin' tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Payment (Walkin)</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>

                          <div class="modal-body">
                            <form method ='post' id ='walkinbilling'>
                              <div class='container-fluid'>
                                <div class='form-group'>
                                  <input type ='number' id ='paymentwalkin' class ='form-control' placeholder = 'Input guest payment here' name = 'payment'/>
                                </div>
                                <hr/>
                                <!-- <div class ='row'>
                                  <div class ='col-md-6' style ='text-align:left'>
                                    <h6>Change</h6>
                                  </div>
                                  <hr/>
                                  <div class ='col-md-6' style ='text-align:right'>
                                    <h6 id = 'changeVal'>None</h6>
                                  </div>
                                </div> -->
                              </div>
                          </div>
                          <div class="modal-footer">
                            <input type ='hidden' name ='r_id'/>
                            <input type ='hidden' name ='currentBalance'/>
                            <button name = 'updatewalkin' type = 'submit' class='btn btn-primary btn-block'>Update</button>
                          </div>
                        </form>

                      </div>
                    </div>
                  </div>



                    <div class="modal fade" id ='addservice' tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Add services</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>

                          <div class="modal-body">
                            <form  method ='post' id = 'addservices' action = ''>
                              <div class='container-fluid'>
                                <div class='form-group'>
                                  <input type ='number' id ='paymentwalkin' class ='form-control' placeholder = 'Input guest payment here' name = 'payment'/>
                                </div>
                                <hr/>
                                <div class ='row'>
                                  <div class ='form-group col-md-9'>
                                  <label>Addons</label>
                                      <select name ='addons' id ='addonsQuantity' class ='form-control'>
                                        <?php
$fetchaddons = mysqli_query($conn, "SELECT * FROM addons_masterfile");
while ($addons = mysqli_fetch_assoc($fetchaddons)) {
    ?>
                                          <option value ='<?=$addons['Addon_ID']?>'><?=$addons['Addon_name']?></option>
                                          <?php
}
?>
                                        <option value ='x' selected>None</option>
                                      </select>
                                  </div>
                                  <div class ='form-group col-md-3'>
                                      <label>Quantity</label>
                                      <select name = 'addonsqty' id = 'addonsqty' class ='form-control'>

                                      </select>
                                  </div>
                                  <hr/>
                              </div>
                            </div>

                          </div>
                          <div class="modal-footer">
                              <input type ='hidden' name ='checkInDate' value =''/>
                              <input type ='hidden' name ='checkOutDate' value =''/>
                              <input type ='hidden' name ='r_id' value =''/>
                              <input type ='hidden' name ='guest_id' value =''/>
                              <input type ='submit' name ='submitservice' class ='btn btn-info btn-block'/>
                          </div>
                        </form>
                        <?php
if (isset($_POST['submitservice'])) {
    if ($_POST['addons'] != 'x') {
        $selectaddon = mysqli_query($conn, "SELECT * FROM guestaddons_masterfile WHERE reservation_id = {$_POST['r_id']} AND addons_id = {$_POST['addons']}") or die(mysqli_error($conn));
        if (mysqli_num_rows($selectaddon) == 0) {
            mysqli_query($conn, "INSERT INTO guestaddons_masterfile(addons_id,reservation_id,quantity) VALUES({$_POST['addons']},{$_POST['r_id']},{$_POST['addonsqty']})") or die(mysqli_error($conn));
        } else {
            mysqli_query($conn, "UPDATE guestaddons_masterfile SET quantity = quantity + {$_POST['addonsqty']} WHERE reservation_id = {$_POST['r_id']} AND addons_id = {$_POST['addons']}") or die(mysqli_error($conn));
        }
        $fetchrate = mysqli_query($conn, "SELECT * FROM addons_masterfile WHERE Addon_ID = {$_POST['addons']}");
        $rate = mysqli_fetch_assoc($fetchrate);
        $total = $rate['Addon_rate'] * $_POST['addonsqty'];
        // add to billing
        mysqli_query($conn, "UPDATE billing_masterfile SET balance = balance + {$total}, total = total + {$total} WHERE reservation_id = {$_POST['r_id']}");
        echo "<script>alert('Success')</script>";
    }
}
?>
                      </div>
                    </div>
                  </div>







<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                    <!-- Edit Modal -->



                    <div class="modal fade" id ='pay' tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Payment (Reservation)</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>

                          <div class="modal-body">
                             <form id="formEditBilling" action ='../ajax/editbilling.php' enctype="multipart/form-data" method ='post'>
                              <div class='container-fluid'>
                                <div class='form-group'>
                                     <input type ='number' step ='0.01' id ='payment' class ='form-control' placeholder = 'Input guest payment here' name = 'payment'/>
                                </div>
                                <hr/>
                                <!-- <div class ='row'>
                                  <hr/>
                                  <div class ='col-md-6' style ='text-align:right'>
                                      <h6 id = 'changeVal'>0.00 PHP</h6>
                                  </div>
                                </div> -->
                              </div>
                          </div>
                          <div class="modal-footer">
                              <input type ='hidden' name ='b_id'/>
                              <input type ='hidden' name ='currentBalance'/>
                              <input type ='hidden' name ='r_id'/>
                              <input type ='hidden' name ='g_id'/>
                              <button name = 'update' type = 'submit' class='btn btn-primary btn-block'>Update</button>
                          </div>
                        </form>

                      </div>
                    </div>
                  </div>





                    <!-- Edit reservation modal -->
                    <div class="modal fade" id ='editreservation1' tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Edit Guest Reservation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <form id="formEditRoom" enctype="multipart/form-data" action = '../ajax/editreservation.php' method ='post'>
                              <div class='container-fluid'>
                                <div class='form-group'>
                                  <table class='table table-bordered' id='dataTable' width='100%' cellspacing='0'>
                                    <label for='RoomType'>Check in</label><br>
                                    <input required class='form-control' name = 'checkin' id = 'checkInDate' type ='date'>
                                    <div class='form-group'>
                                      <label for='roomRate'>Check out</label><br>
                                      <input required class='form-control' name = 'checkout'  id = 'checkOutDate' type='date'>
                                    </div>
                                    <div class='form-group'>
                                      <label for='roomNumber'>Room Type</label><br>
                                      <select class ='form-control' name ='roomtype' id ='roomtype' REQUIRED>
                                        <?php
$fetchrooms = mysqli_query($conn, "SELECT * FROM room_masterfile");
while ($row = mysqli_fetch_assoc($fetchrooms)) {
    echo "<option class ='get' value = '{$row['room_id']}'>{$row['room_type']}</option>";
}
?>
                                      </select>
                                    </div>
                                    <div class='form-group'>
                                      <label for='roomRate'>Room Quantity</label><br>
                                      <select id ='roomquantity' name = 'roomquantity' class ='form-control' REQUIRED>

                                      </select>
                                    </div>
                                    <input type ='hidden' name = 'reservationno'/>
                                    <input type ='hidden' name ='decrease'/>
                                  </table>
                                  <h5>Addons</h5>
                                  <hr/>
                                  <div class ='row' id ='addons'>

                                  </div>
                                </div>
                              </div>

                            </div>
                            <div class="modal-footer">
                              <input type ='hidden' name = 'roomId'>
                              <button name = 'update' type = 'submit' class='btn btn-primary btn-block'>Update Room</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <!-- End edit reservation modal -->
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>




<!-- Bootstrap core JavaScript-->
<!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
<!-- <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<!-- Core plugin JavaScript-->
<!-- <script src="../vendor/jquery-easing/jquery.easing.min.js"></script> -->
<!-- Page level plugin JavaScript-->
<!-- <script src="vendor/chart.js/Chart.min.js"></script> -->

<script type = 'text/javascript' src ='../js/datatables.min.js'></script>
<script type="text/javascript" charset="utf8" src="../js/dataTables.bootstrap.min.js"></script>
<script src = "../vendor/datepicker/jquery.datetimepicker.full.min.js"></script>
<script>
  $(document).ready(function() {
      $('#tablebilling').DataTable({
          "paging": false
      })
      $('#tablewalkin').DataTable({
          "paging": false
      })
      // Add service
      		$('.paymentmodalwalkin').click(function() {
			var total = $(this).closest('tr').find('#balance').html().replace(/\,/, '')
			var r_id = $(this).closest('tr').find('#reservation-id').html()
			$('input[name=currentBalance]').val(total)
			$('input[name=r_id]').val(r_id)
		})

		$('#paymentwalkin').keyup(function() {
			String.prototype.number_format = function(d) {
				var n = this;
				var c = isNaN(d = Math.abs(d)) ? 2 : d;
				var s = n < 0 ? "-" : "";
				var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
				return s + (j ? i.substr(0, j) + ',' : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + ',') + (c ? '.' + Math.abs(n - i).toFixed(c).slice(2) : "");
			}
			var payment = parseFloat($(this).val())
			var balance = parseFloat($('input[name=currentBalance]').val())
			// $('#changeVal').html("None")
			// if (balance < payment) {
			// 	$('#changeVal').html((payment - balance).toString().number_format() + " PHP")
			// }
		})
      $('#walkin').click(function(){
          $(this).css('color','white')
          $('#online').css('color','black');
          $(this).parent().css('background-color','#DC143C')
          $('#onlinecard').css('background-color','#FA8072')
          $('#billingwalkin').fadeIn()
          $('#billingreservation').fadeOut()
      })
      $('#online').click(function(){
          $(this).css('color','white')
          $('#walkin').css('color','black')
          $(this).parent().css('background-color','#DC143C')
          $('#walkincard').css('background-color','#FA8072')
          $('#billingwalkin').fadeOut()
          $('#billingreservation').fadeIn()
      })
      $('#checkInDate, #checkOutDate, #roomtype').on('change',function(){
        $.ajax({
          type:'POST',
          url:'../ajax/getreservedrooms.php',
          data:{
            checkInDate: $('#checkInDate').val(),
            checkOutDate: $('#checkOutDate').val(),
            room_id: $('option.get:selected').val(),
            reservation_id: $('input[name=reservationno]').val(),
          },
          success:function(html){
            var availablerooms = parseInt(html)
            var decrease = Math.floor(parseInt($('input[name=decrease]').val()))
            $('#roomquantity').empty()
            for(var x = 1 + decrease ; x<=availablerooms; x++){
              $('#roomquantity').append(`<option value = ${x}>${x}</option>`)
            }
          },
          error:function(html){
            alert(html)
          }
        })
      })
      $('.paymentmodal').click(function() {
        var b_id = $(this).closest('tr').find('#billing-id').html()
        var total = $(this).closest('tr').find('#balance').html().replace(/\,/, '')
        var r_id = $(this).closest('tr').find('#reservation-id').html()
        var g_id = $(this).closest('tr').find('#guest-id').html()
        $('input[name=g_id]').val(g_id)
        $('input[name=b_id]').val(b_id)
        $('input[name=currentBalance]').val(total)
        $('input[name=r_id]').val(r_id)
      })
      $('#addonsQuantity').change(function() {
        $.ajax({
          type: 'POST',
          url: '../ajax/getalladdons.php',
          data: {
            checkInDate: $('input[name=checkInDate]').val(),
            checkOutDate: $('input[name=checkOutDate').val(),
            a_id: $(this).val()
          },
          success: function(html) {
            var addons = parseInt(html)
            $('#addonsqty').empty()

            for (var x = 1; x <= addons; x++) {
              $('#addonsqty').append(`<option value = '${x}'>${x}</option>`)
            }
          },
          error: function(html){
            alert(`${html} rooms not found`)
          }
        })
      })
      $('.addservice').click(function() {
        $('#addonsQuantity option').last().remove()
        $('#addonsQuantity').append(`<option value ='x' selected >None</option>`)
        var checkInDate = $(this).closest('tr').find('#checkin').html()
        var checkOutDate = $(this).closest('tr').find('#checkout').html()
        var r_id =  $(this).closest('tr').find('#reservation-id').html()
        var g_id = $(this).closest('tr').find('#guest-id').html()
        $('input[name=r_id]').val(r_id)
        $('input[name=checkInDate').val(checkInDate)
        $('input[name=checkOutDate').val(checkOutDate)
        $('input[name=guest_id]').val(g_id)
      })
      //End service
      // Edit reservation
      $('.editreservation').on('click', function() {
        var checkin = $(this).parent().closest('tr').find('#checkin').html()
        var checkout = $(this).closest('tr').find('#checkout').html()
        var reservationno = $(this).closest('tr').find('#reservation-id').html()
        var roomno = $(this).closest('tr').find('#room-number').html()
        var decrease = $(this).closest('form').find('input[name=decrease]').val()
        $.ajax({
          type: 'POST',
          url: '../ajax/assigncheckin.php',
          data: {
            checkin: checkin,
            checkout: checkout
          },
          success: function(html) {
            $('#addons').html(html)
          },
          error:function(){
            alert('error')
          }
        })
        $('input[name=checkin]').val(checkin)
        $('input[name=checkout').val(checkout)
        $('input[name=roomquantity').val(roomno)
        $('input[name=reservationno]').val(reservationno)
        $('input[name=decrease]').val(decrease)
      })
      // End edit reservation
      $('form').on('submit', function(e) {
        var message = ''
        var prompt = true
        var walkin = false
        var formId = $(this).attr('id')
        if (formId == 'formEditBilling') {
                    e.preventDefault()
          message = "Billing has been updated"
                  alert(message)
        } else if(formId == 'formDeleteBilling') {
                    e.preventDefault()
          var prompt = confirm("Are you sure?")
          message = "Billing has been deleted"
                  alert(message)
        }
        else if (formId == 'addservice'){
                    e.preventDefault()
          message ="Services has been updated"
                  alert(message)
        }
        else if(formId =='formEditRoom'){
                    e.preventDefault()
          message = 'Room has been edited'

        }
        if (prompt) {
          $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(html) {
                if(formId =='formEditRoom'){
                    var json = $.parseJSON(html)
                    if(json.error !== undefined){
                        alert(json.error)
                    }
                    else{
                        alert(message)
                    }
                }
                else{
                    alert(message)
                }
              location.reload()
            }
          })
        }
      })
      $('#payment').keyup(function() {
        String.prototype.number_format = function(d) {
          var n = this;
          var c = isNaN(d = Math.abs(d)) ? 2 : d;
          var s = n < 0 ? "-" : "";
          var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
          return s + (j ? i.substr(0, j) + ',' : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + ',') + (c ? '.' + Math.abs(n - i).toFixed(c).slice(2) : "");
        }
        var payment = parseFloat($(this).val())
        var balance = parseFloat($('input[name=currentBalance]').val())
        // $('#changeVal').html("0.00 PHP")
        // if (balance < payment) {
        //   $('#changeVal').html((payment - balance).toString().number_format() + " PHP")
        // }
      })
      $("#checkInDate").datetimepicker({
        timepicker: false,
        format: "Y-m-d",
        onShow: function(ct) {
          this.setOptions({
            minDate: $('#checkInDate').val()
          })
        }
      })

      $("#checkOutDate").datetimepicker({
        timepicker: false,
        format: "Y-m-d",
        onShow: function(ct) {
          this.setOptions({
            minDate: `+$('#checkInDate').val()` ? $('#checkInDate').val() : false
          })
        }
      })
    })
  </script>
</body>

</html>
