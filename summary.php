<?php
include_once 'db.php';
include_once 'header.php';
session_start();
$_FILES = array();
$_POST = array();
$fetchCurrentReservation = mysqli_query($conn, "SELECT * FROM reservation_masterfile WHERE reservation_id = {$_GET['code']} AND guest_id = {$_SESSION['guest_ID']}");
$reservation = mysqli_fetch_assoc($fetchCurrentReservation);
if(mysqli_num_rows($fetchCurrentReservation) == 0){
  echo "<script>alert(\"Reservation not found.\")
  window.href.location = 'GuestDashboard.php'
  </script>
  ";
}
$daydiff = (strtotime($reservation['checkoutdate']) - strtotime($reservation['checkindate']))/(60*60*24);
$fetchCurrentRoom = mysqli_query($conn, "SELECT * FROM room_masterfile WHERE room_id = {$reservation['room_id']}") or die(mysqli_error($conn));
$room = mysqli_fetch_assoc($fetchCurrentRoom);
$fetchbilling = mysqli_query($conn, "SELECT * FROM billing_masterfile WHERE reservation_id = {$_GET['code']} AND guest_id = {$_SESSION['guest_ID']}");
$billing = mysqli_fetch_assoc($fetchbilling);
$fetchAddons = mysqli_query($conn, "SELECT * FROM guestaddons_masterfile as gAddon INNER JOIN addons_masterfile as Addon on gAddon.addons_id = Addon.Addon_id WHERE gAddon.reservation_id = {$reservation['reservation_id']}") or die(mysqli_error($conn));
$addonTotal = 0;
while($addon = mysqli_fetch_assoc($fetchAddons)){
  $addonTotal += $addon['Addon_rate'];
}

?>

<style>
.navbar-brand{
  margin-top: -22px;
  margin-right: 5px;
}
.btn-primary {
  color: #fff;
  background-color: #96281B;
  border-color: #96281B;
}

  .table-condensed>tbody>tr>td{
  padding: 5px;
}
.table>tbody>tr>td {
  padding: 8px;
  line-height: 1.42857143;
  vertical-align: top;
  border-top: 1px solid #ddd;
}
.text-left {
  text-align: left;
}
table {
  background-color: transparent;
}
table {
  border-spacing: 0;
  border-collapse: collapse;
}
.table-responsive {
  min-height: .01%;
  overflow-x: auto;
}
.table-responsive {
  min-height: .01%;
  overflow-x: auto;
}
.panel {
  margin-bottom: 20px;
  background-color: #fff;
  border: 1px solid transparent;
  border-radius: 4px;
  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
  box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 20px;
}
.panel-body {
  padding: 15px;
}
.table>thead:first-child>tr:first-child>td {
  border-top: 0;
}
.table-condensed>tfoot>tr>th{
  padding: 5px;
}
.panel-default {
  border-color: #ddd;
}
.panel-body {
  padding: 15px;
}
</style>
<body>
 <?php
    include_once 'navigationBar.php';
 ?>
  <br>
  <br>
  <br>
  <br>
  <div class = "container">
    <div class = "row" style = "margin-bottom: 10%">
      <div class = "col-xs-12">
        <button class="btn-hide btn btn-success pull-right hide-this-shit" onclick="window.print()" style="margin-right:30px; margin-top:-35px;">Print</button>
        <div class = "col-md-12">
          <!-- policy -->
          <div class = "col-md-4">
            <div class = "panel panel-default">
              <div class = "panel-heading" style = "background-color: ;">
                <h3 class = "text-center">
                  <b style = "color: white;">Policy</b>
                </h3>
              </div>
              <div class = "panel-body __web-inspector-hidebefore-shortcut__">
                <div class ="table-responsive">
                  <table class = "table table-condensed">
                    <thead>
                      <tr>
                        <td class = "text-left">
                          <b></b>
                        </td>
                        <td class = "text-left"></td>
                      </tr>
                    </thead>
                    <tbody style = "color: black; width:277px;">
                      <tr>
                        <td class = "text-left" bgcolor = "#EBEDF2">Pre-Payment</td>
                        <td class = "text-left" bgcolor = "#EBEDF2" style = 'text-align: justify'>
                          As per hotel policy, you are to pay your 15% downpayment to our bank account as deposit, 
                          take a screenshot of it and upload it to your customer dashboard. When you have successfully uploaded the Hotel will monitor your records and confirm your reservation. All pending reservations must be settled within 3 days, else it will expire. 
                        </td>
                      </tr>
                      <tr>
                        <td class = "text-left" bgcolor = "#EBEDF2">Cancelling</td>
                        <td class = "text-left" bgcolor = "#EBEDF2" style = 'text-align: justify'>
                          You can cancel your pending reservations using the customer dashboard.
                        </td>
                      </tr>
                      <tr>
                        <td class = "text-left" bgcolor = "#EBEDF2">Rebooking</td>
                        <td class = "text-left" bgcolor = "#EBEDF2" style = 'text-align: justify'>
                          You can modify your pending reservations using the customer dashboard.
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- guest info -->
          <div class = "col-md-8">
            <div class = "panel panel-default">
              <div class = "panel-heading" style = "background-color: ">
                <h3 class = "text-center">
                  <b style = "color: white;">Guest Information</b>
                </h3>
              </div>
              <div class = "panel-body">
                <div class = "table-responsive">
                  <table class = "table table-condensed">
                    <thead>
                    </thead>
                    <tbody style = "color: black;">
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left">
                          <b>Name:</b>
                        </td>
                        <td class = "text-left" bgcolor = "#EBEDF2">
                          <?= "{$_SESSION['firstname']} {$_SESSION['lastname']}" ?>
                        </td>
                      </tr>
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left">
                          <b>Email</b>
                        </td>
                        <td class = "text-left" bgcolor= "#EBEDF2">
                          <?= $_SESSION['email'] ?>
                        </td>
                      </tr>
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left">
                          <b>Cellphone:</b>
                        </td>
                        <td class = "text-left" bgcolor= "#EBEDF2">
                          <?= $_SESSION['contactNumber'] ?>
                        </td>
                      </tr>
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left">
                          <b>Address:</b>
                        </td>
                        <td class = "text-left" bgcolor= "#EBEDF2">
                          <?= $_SESSION['address'] ?>
                        </td>
                      </tr>
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left">
                          <b>Check-in</b>
                        </td>
                        <td class = "text-left" bgcolor= "#EBEDF2">
                          <?= $reservation['checkindate'] ?>
                        </td>
                      </tr>
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left">
                          <b>Check out</b>
                        </td>
                        <td class = "text-left" bgcolor= "#EBEDF2">
                          <?= $reservation['checkoutdate'] ?>
                        </td>
                      </tr>
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left">
                          <b>Number of Guest</b>
                        </td>
                        <td class = "text-left" bgcolor= "#EBEDF2">
                          <?= $reservation['number_guest'] ?>
                        </td>
                      </tr>
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left">
                          <b>Extra Services:</b>
                        </td>
                        <td class = "text-left" bgcolor= "#EBEDF2">

                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- billing -->
          <div class = "col-md-8">
            <div class = "panel panel-default">
              <div class = "panel-heading">
                <h3 class = "text-center">
                  <b style = "color: white;">Total Billing</b>
                </h3>
              </div>
              <div class = "panel-body" stlye = "margin-bottom: -25px">
                <div class = "table-responsive">
                  <table class = "table table-condensed">
                    <thead style = "color: black;">
                      <tr>
                        <td bgcolor = "#EBEDF2" class = "text-left"><b></b></td>
                        <td bgcolor = "#EBEDF2" class = "text-left"><b></b></td>
                        <td class = "text-center" bgcolor="#EBEDF2">
                          <b>Number of Room/s</b>
                        </td>
                        <td bgcolor = "#EBEDF2" class = "text-right">
                          <b>Cost</b>
                        </td>
                      </tr>
                    </thead>
                    <tbody style = "color: black;"> 
                      <tr>
                        <td class="text-left" bgcolor="#EBEDF2">
                          <b>Type of Room</b>
                        </td>
                        <td class="text-left" bgcolor="#EBEDF2">
                          <?= $room['room_type'] ?>
                        </td> 
                        <td class="text-center" bgcolor="#EBEDF2">
                          <?= $reservation['room_number']?>
                        </td>
                        <td class="text-right" bgcolor="#EBEDF2">
                          <?=number_format($room['room_rate'] * $reservation['room_number'],2)?> php
                        </td>
                      </tr>
                      <tr>
                        <td class="text-left" bgcolor="#EBEDF2">
                          <b>Length of stay</b>
                        </td>
                        <td class="text-left" bgcolor="#EBEDF2">
                          <?= $daydiff ?>
                        </td>
                        <td class="text-left" bgcolor="#EBEDF2">

                        </td>
                        <td class="text-right" bgcolor="#EBEDF2">

                        </td>
                      </tr>
                      
                      <tr>
                        <td class ='highrow text-left' bgcolor="#EBEDF2">
                          <b>Extra Services</b>
                        </td>
                        <td class ='highrow text-left' bgcolor="#EBEDF2"></td>
                        <td class ='highrow text-center' bgcolor="#EBEDF2"></td>
                        <td class ='highrow text-right' bgcolor="#EBEDF2"></td>
                      </tr>
                      <?php
                        $fetchAddons1 = mysqli_query($conn, "SELECT * FROM guestaddons_masterfile as gAddon INNER JOIN addons_masterfile as Addon on gAddon.addons_id = Addon.Addon_id WHERE gAddon.reservation_id = {$reservation['reservation_id']}") or die(mysqli_error($conn));
                        while($addon = mysqli_fetch_assoc($fetchAddons1)){
                          ?>
                          <tr>
                            <td class ='highrow text-left' bgcolor="#EBEDF2">
                            </td>
                            <td class ='highrow text-left' bgcolor="#EBEDF2"></td>
                            <td class ='highrow text-center' bgcolor="#EBEDF2"><?=$addon['Addon_name'] . " ({$addon['quantity']})"?></td>
                            <td class ='highrow text-right'bgcolor="#EBEDF2"><?=number_format($addon['Addon_rate'] * $addon['quantity'],2)?> PHP</td>
                          </tr>
                          <?php } ?>
                                        <tr>
                        <td class="highrow text-left" bgcolor="#EBEDF2">

                        </td>
                        <td class="highrow text-left" bgcolor="#EBEDF2">

                        </td>
                        <td class="highrow text-center" bgcolor="#EBEDF2">
                          <b>Subtotal (taxed to 12%)</b>
                        </td>
                        <td class="highrow text-right" bgcolor="#EBEDF2">
                         <?= number_format(($billing['total'])*0.88,2). " php"?>
                       </td>
                     </tr>
                     <tr>
                      <td class="text-left" bgcolor="#EBEDF2">

                      </td>
                      <td class="text-left" bgcolor="#EBEDF2">

                      </td>
                      <td class="text-center" bgcolor="#EBEDF2">
                        <b>Vat 12%</b>
                      </td>
                      <td class="text-right" bgcolor="#EBEDF2">
                        <?= number_format($billing['total']* 0.12,2) ?> php
                      </td>
                    </tr>
                          <?php 
                        
                        if($reservation['discounted']) { ?>
                                 <tr>
                      <td class="text-left" bgcolor="#EBEDF2">

                      </td>
                      <td class="text-left" bgcolor="#EBEDF2">

                      </td>
                      <td class=" highrow text-center" bgcolor="#EBEDF2">
                        <b>Original Amount</b>
                      </td>
                      <td class=" highrow text-right" bgcolor="#EBEDF2">
                        <?= number_format($billing['total']/0.90, 2)?> php
                      </td>
                    </tr>
                    <?php } ?>
                    <tr>
                      <td class="text-left" bgcolor="#EBEDF2">

                      </td>
                      <td class="text-left" bgcolor="#EBEDF2">

                      </td>
                      <td class=" highrow text-center" bgcolor="#EBEDF2">
                          <?php if($reservation['discounted']) {?>
                        <b>Discounted</b>
                      <td class=" highrow text-right" bgcolor="#EBEDF2">
                        <?= number_format($billing['total']/0.90,2) . " - 10%  = " .number_format($billing['total'], 2)?> php
                      </td>
                        <?php }
                        else{ ?>
                        <b>Total</b>
                      <td class=" highrow text-right" bgcolor="#EBEDF2">
                        <?= number_format($billing['total'], 2)?> php
                      </td>
                        <?php } ?>
                      </td>
 
                    </tr>
                                   <tr>
                      <td class="text-left" bgcolor="#EBEDF2">

                      </td>
                      <td class=" text-left" bgcolor="#EBEDF2">

                      </td>
                      <td class=" text-center" bgcolor="#EBEDF2">
                        <b>Down payment</b>
                      </td>
                      <td class=" text-right" bgcolor="#EBEDF2">
                        <?= number_format($billing['total'], 2) . " * 15% = ". number_format($billing['total'] * 0.15, 2)?> php
                      </td>
                    </tr>
        
     
      
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <h3 style = "color:black;">Payment Proof:</h3>
          <form id = "upload" class="hide-this-shit hidden-xs" method="post" style="display:inline; float:right !important;" enctype="multipart/form-data">
            <input class="hide-this-shit hidden-xs" type="file" name="img" style="display:inline;">
            <input type ='hidden' name = 'code' value = '<?= $_GET['code']?>'/>
            <a class="hide-this-shit hidden-xs" id = 'myBtn' name="upload" style="display:inline; background:#cc0000; font-weight:bold; color:white;" >
              Upload
            </a> 
          </form>
        </div>
        <br>
        <br>
        <br>
        <br>
        <center>
             <h5>
            For PayPal Payments please <a href="http://hometownhotelmakati.com/Paypal.php">Click Here</a>.
          </h5>
         </center>
         
         <!-- Modal Save Changes -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
            <!-- Modal content-->
              <div class="modal-content" style = "padding: 0px;">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Save Changes?</h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure you want to save changes?.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" id = 'btnYes' data-dismiss="modal">Yes</button>
                  
                  <button type="button" class="btn btn-default" id = 'btnNo' data-dismiss="modal">No</button>
                </div>
              </div>
            </div>
          </div>
          
          <!-- Modal Success -->
          <div class="modal fade" id="modalSuccess" role="dialog">
            <div class="modal-dialog">
            
            <!-- Modal content-->
              <div class="modal-content" style = "padding: 0px;">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <!--<h4 class="modal-title">Save Changes?</h4>-->
                </div>
                <div class="modal-body">
                  <p>Your proof of payment was successfully uploaded. Wait for the admin to approved your reservation. Thank you!</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

        <!--<script>-->
        <!--$(document).ready(function(){-->
        <!--    $("#myBtn").click(function(){-->
        <!--        $("#myModal").modal();-->
        <!--    });-->
        <!--});-->
        <!--</script>-->

        <!-- Latest jQuery plugin-->
        <script src="js/main.js"></script>
        <!-- Latest compiled and minified JavaScript for bootstrap-->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/parallax.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpKAwq-qKxzm-9D1405KCFp7ZTtu_Vimg"></script>
        <script src="js/googleMap.js"></script>
        <script src="js/customGoogleMap.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.timepicker.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/jquery.flexslider-min.js"></script>
        <script src="js/select2.min.js"></script>
        <script src="js/icheck.min.js"></script>
        <script src="js/jquery.raty.js"></script>
        <script src="js/jquery.datetimepicker.full.min.js"></script>
        <script src="js/scripts.js"></script>
        <script>
          $(document).ready(function(){

            $("#myBtn").click(function(){
                $("#myModal").modal();
            });
            $("#btnYes").click(function(){
                $("#upload").submit();

            });
            $('form').on('submit',function(e){
              var form_data = new FormData()
              form_data.append('img', $('input[type=file]').prop('files')[0])
              if($('input[type=file]').prop('files')[0] == undefined ){
                    alert('Please upload an image')
                    return false
                }
              form_data.append('code', $('input[name=code]').val())
              $.ajax({
                url:'ajax/uploadproof.php',
                type:'post',
                data:form_data,
                contentType: false,
                processData: false,
                success:function(html){
                   $("#modalSuccess").modal();
                },
                error: function(html){
                    alert(html)
                }
              
              })
            
            })
          })
        </script>
      </body>
      </html>